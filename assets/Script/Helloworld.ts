import { GG } from "./Global";
import Item from "./Item";
import ScrollOption, { CellTranslationDirect, ScrollOptionCell } from "./ScrollOption";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Helloworld extends cc.Component {

    onLoad() {
        this.lbl_tip.string = "";
    }

    start() {
        // 初始化纵向列表
        this.node_1.getChildByName("scrollOption")!.getComponent(ScrollOption).init({
            getSpacing: (): number => {
                return 2;
            },
            getCellSize: (): Array<number> => {
                const node: cc.Node = cc.instantiate(this.prefab_item);
                return [node.width, node.height];
            },
            getCellNumber: (): number => {
                return this.BTNS.length;
            },
            getCellView: (): BtnScrollCel => {
                return cc.instantiate(this.prefab_item)!.addComponent(BtnScrollCel);
            },
            getCellData: (): any => {
                return this.BTNS;
            },
            getCellScale: (): number => {
                return 2;
            },
            getTranslation: (): Array<number> => {
                return [CellTranslationDirect.RIGHT_OR_TOP, 2.5];
            },
            getDefaultLoaction: (): number => {
                return 3;
            },
            script: this
        });

        // 初始化横向列表
        this.node_2.getChildByName("scrollOption")!.getComponent(ScrollOption).init({
            getSpacing: (): number => {
                return 2;
            },
            getCellSize: (): Array<number> => {
                const node: cc.Node = cc.instantiate(this.prefab_item);
                return [node.width, node.height];
            },
            getCellNumber: (): number => {
                return this.BTNS.length;
            },
            getCellView: (): BtnScrollCel => {
                return cc.instantiate(this.prefab_item)!.addComponent(BtnScrollCel);
            },
            getCellData: (): any => {
                return this.BTNS;
            },
            getCellScale: (): number => {
                return 2;
            },
            getTranslation: (): Array<number> => {  // 设置cell偏移方向和偏移系数，系数为0不偏移
                return [CellTranslationDirect.LEFT_OR_BOTTOM, 0];
            },
            getDefaultLoaction: (): number => {
                return 3;
            },
            script: this
        });
    }

    /**
     * 按钮事件
     * @param evt 
     */
    onMainBtnToDo(evt: string) {
        if(evt == "") {
            this.lbl_tip.string = "无事件";
            return;
        }

        this[evt]();
    }

    onBtnToDo1(){
        this.lbl_tip.string = "按钮事件1";
    }

    onBtnToDo2(){
        this.lbl_tip.string = "按钮事件2";
    }

    onEnable() {
        GG.eventBus.on(Item.EVENT_MAIN_BTN, this.onMainBtnToDo, this);
    }

    onDisable() {
        GG.eventBus.off(Item.EVENT_MAIN_BTN, this.onMainBtnToDo, this);
    }

    BTNS = [
        {
            name: "btn1",
            lbl: "1",
            evt: "onBtnToDo1",
            icon: "",
            desc: ""
        },
        {
            name: "2",
            lbl: "2",
            evt: "onBtnToDo2",
            icon: "",
            desc: ""
        },
        {
            name: "3",
            lbl: "3",
            evt: "",
            icon: "",
            desc: ""
        },
        {
            name: "4",
            lbl: "4",
            evt: "",
            icon: "",
            desc: ""
        },
        {
            name: "5",
            lbl: "5",
            evt: "",
            icon: "",
            desc: ""
        },
        {
            name: "6",
            lbl: "6",
            evt: "",
            icon: "",
            desc: ""
        },
        {
            name: "7",
            lbl: "7",
            evt: "",
            icon: "",
            desc: ""
        },
        {
            name: "8",
            lbl: "8",
            evt: "",
            icon: "",
            desc: ""
        },
        {
            name: "9",
            lbl: "9",
            evt: "",
            icon: "",
            desc: ""
        },
        {
            name: "10",
            lbl: "10",
            evt: "",
            icon: "",
            desc: ""
        }
    ];

    @property(cc.Node)
    node_1: cc.Node = null;
    @property(cc.Node)
    node_2: cc.Node = null;

    @property(cc.Label)
    lbl_tip: cc.Label = null;

    @property(cc.Prefab)
    prefab_item: cc.Prefab = null;
}

/**
 * 滚动菜单
 */
class BtnScrollCel extends ScrollOptionCell {
    public updateContent(data: any) {
        if (!data) return;
        this.node.getComponent(Item).init(data[this.dataIndex]);
        this.node.getComponent(Item).enableOrDisableBtn();
        this.node.name = "cell_" + this.dataIndex;
        this.node.getComponent(cc.Widget).enabled = true;
        this.node.opacity = 100;
    }

    public enableCell(data: any) {
        this.node.opacity = 255;
        this.node.getComponent(Item).enableOrDisableBtn(true);
        if (this.showed) return;
        this.showed = true;
        // 显示详情等
        // this.script.onBtn2ShowBtnDesc(data[this.dataIndex].desc);
    }

    public disableCell() {
        this.node.opacity = 100;
        this.node.getComponent(Item).enableOrDisableBtn();
        this.showed = false;
    }
}
