import { GG } from "./Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Item extends cc.Component {

    onLoad() {
        this._localZOrder = this.node.zIndex;
    }

    init(data: any) {
        this.data = data;
        this.lbl_text.string = this.data.lbl;
    }

    /**
     * button事件
     */
    onBtn2DoSth() {
        GG.eventBus.emit(Item.EVENT_MAIN_BTN, this.data.evt);
    }

    /**
     * 启用或禁用按钮
     * @param enable 是否启用
     */
    enableOrDisableBtn(enable: boolean = false) {
        this.node.getComponent(cc.Button).interactable = enable;
    }

    /**
     * 提升或恢复节点层级
     * @param add 
     */
    addOrResumeLocalZOrder(add: boolean = false) {
        if (add) {
            this.node.zIndex = 1;
            return;
        }
        this.node.zIndex = this._localZOrder;
    }

    static EVENT_MAIN_BTN: string = "EVENT_MAIN_BTN";

    data: any = {

    };
    /**
     * cell节点默认层级
     */
    private _localZOrder: number = 0;

    @property(cc.Node)
    node_btn: cc.Node = null;
    @property(cc.Label)
    lbl_text: cc.Label = null;
}